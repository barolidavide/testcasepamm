# Copyright (C) 2015-2019 by the RBniCS authors
#
# This file is part of RBniCS.
#
# RBniCS is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# RBniCS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with RBniCS. If not, see <http://www.gnu.org/licenses/>.
#

# TODO NonlinearTimeDependentPODGalerkinReducedProblem (should be)
# but we need to move to all time dependent (how it change ABC CFD?)
# FB: yeah, the ABC CFD is a bit of hack, I am not fully satisfied with it. If we find a better way
#     to reuse that code I am open to changing it.

from rbnics.problems.base import NonlinearPODGalerkinReducedProblem, ParametrizedReducedDifferentialProblem

from rbnics.utils.decorators import ReducedProblemFor


# TODO: create a folder reduction_methods FB: I believe this is done now
from reduction_methods import PseudoNavierStokesPODGalerkinReduction

from .pseudo_navier_stokes_reduced_problem import PseudoNavierStokesReducedProblem
from .pseudo_navier_stokes_problem import PseudoNavierStokesProblem


PseudoNavierStokesPODGalerkinReducedProblem_Base = NonlinearPODGalerkinReducedProblem(PseudoNavierStokesReducedProblem(ParametrizedReducedDifferentialProblem))

@ReducedProblemFor(PseudoNavierStokesProblem, PseudoNavierStokesPODGalerkinReduction)
class PseudoNavierStokesPODGalerkinReducedProblem(PseudoNavierStokesPODGalerkinReducedProblem_Base):
    pass
