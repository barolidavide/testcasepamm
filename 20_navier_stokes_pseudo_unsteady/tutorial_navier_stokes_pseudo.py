from dolfin import *
from rbnics import *
from problems import *
from reduction_methods import *
from ufl import inv, det, as_vector, as_matrix


# TODO FB: watch out that you NEED to have an  here, see issue 11 on RBniCS repo
@ExactParametrizedFunctions("offline")
class PseudoNavierStokes(PseudoNavierStokesProblem):
    def __init__(self, W, **kwargs):
        PseudoNavierStokesProblem.__init__(self, M, **kwargs)
        # ... and also store FEniCS data structures for assembly
        self.M = M
        self.subdomains, self.boundaries = kwargs["subdomains"], kwargs["boundaries"]
        psiq = TrialFunction(M)

        (self.v, self.p, self.w, self.Sx, self.Sy, self.Sxy) = split(psiq)
        psip = TestFunction(M)
        (self.δv, self.δp, self.δw, self.δSx, self.δSy, self.δSxy) = split(psip)
        #psid = Function(M)
        #(_, _, self.d, _,_,_) = split(psi_d)
        # I need for convective term
        (self.u_, _, self.w_, _, _, _) = split(self._solution)
        # d w /dt  = d
        # in an approx we have  d =  w_0 + (w-w_0)*dt

        # self.d.assign(self.w)
        
        self.lid = 1
        self.outlet = 2
        self.wall = 3
        self.mid = 4
        self.side = 5
        self.bottom = 6
        self.elfluid = 0
        self.force = Constant((0.0, 0.0))
        # Boundary stress vector
        self.traction = Constant((0.0, 0.0))

        # Boundary velocity at walls
        self.v_wall = Constant((0.0, 0.0))
        # Boundary velocity at lid
        #self.v_lid = Expression(( '1.0*tanh(t)', '0.0' ), t=0.0, degree=2)
        self.v_lid = Expression(('1- (2*x[0]-1)*(2*x[0]-1) ', '0.0'), degree=2)

        # Boundary pressure at pressure point
        self.p_point = Constant(0.0)
        # Mesh velocity at mid-slice
        # self.w_mid = Expression(( '0.1*cos(t)', '0.0' ), t=0.0, degree=2)
        # self.w_mid = Expression(( '0.1*cos(t)*2.0/(h*h)*(h-x[1])*(x[1])', '0.0' ), t=0.0, h=h_, degree=2)
        # self.w_mid = Expression(( '0.075*(1-tanh(t/2.))', '0.0' ), t=0.0, degree=2)
        #self.w_mid = Expression(('0.25*cos(2*t)','0'), t=0.0, degree=2)
        # self.w_mid = Expression(('0.18*(1-tanh(t))','0'), t=0.0, degree=2)
        self.w_mid = Expression(('0.01', '0'), degree=2)

        self.dx = Measure("dx")(subdomain_data=self.subdomains)
        self.ds = Measure("ds")(subdomain_data=self.boundaries)

    def name(self):
        return "TestCase"

    # @compute_theta_for_supremizers

    def compute_theta(self, term):
        mu = self.mu

        if term == "m":
            theta_m1 = mu[0]
            theta_m2 = 1.

            return (theta_m1,theta_m2,)

        if term == "a":
            theta_a1 = 1.
            return (theta_a1,)

        elif term == "bt":
            theta_bt = 1.
            return (theta_bt,)

        elif term == "b":
            theta_b = 2*mu[1]
            return (theta_b,)

        elif term in ("c", "dc"):
            theta_c = mu[0]

            return (theta_c,)

        elif term == "f":
            theta_f0 = 1.
            return (theta_f0,)

        elif term == "g":
            theta_g0 = 1.
            return (theta_g0,)

        elif term == "dirichlet_bc_vel":
            theta_f0 = 1.
            return (theta_f0,)

        # DONE FB: watch out that there should be a "dirichlet_bc_vel" case here, otherwise the v_lid velocity will be homogenized
        else:
            raise ValueError("Invalid term for compute_theta().")

    # @assemble_operator_for_supremizers

    def assemble_operator(self, term):
        dx = self.dx
        ds = self.ds

        v = self.v
        p = self.p
        w = self.w

        S = as_matrix([[self.Sx, self.Sxy], [self.Sxy, self.Sy]])

        δv = self.δv
        δp = self.δp
        δw = self.δw
        δS = as_matrix([[self.δSx, self.δSxy], [self.δSxy, self.δSy]])

        #d = self.d
        #d = self.w_

        traction = self.traction
        force = self.force
        
        E = 1.0
        nu = 0.0
        self.mu_ = E/(2*(1 + nu))
        self.la = E*nu/((1 + nu)*(1 - 2*nu))
        W = (1/(4*self.mu_))*inner(S,S) - 1/2*(self.la/(2*self.mu_*(3*self.la+2*self.mu_)))*tr(S)**2
        dot_S = diff(W,variable(S))
        
        I = Identity(self.w_.geometric_dimension())
        F = I + grad(self.w_*0.5)
        Finv = inv(F)
        J = det(F)

        def dotEv(v, u):
            d = v.geometric_dimension()
            # Identity tensor
            I = Identity(d)
            # Deformation gradient
            F = I + grad(u)
            # Rate of deformation gradient
            dotF = grad(v)
            # Rate of GL strain
            dotE = (dotF.T*F + F.T*dotF) / 2
            #
            return dotE

        def δdotEv(d_v, u):
            d = d_v.geometric_dimension()
            # Identity tensor
            I = Identity(d)
            # Deformation gradient
            F = I + grad(u)
            # Variation of rate of F
            d_dotF = grad(d_v)
            # Variation of rate of GL strain
            d_dotE = (d_dotF.T*F + F.T*d_dotF) / 2
            return d_dotE
        


        def D(v, Finv):
            return (Finv.T * grad(v).T + grad(v) * Finv) / 2

        if term == "m":
            m1 = inner(δv,J*v) * dx
            m2 = inner(δS, dot_S)*dx 
            return (m1,m2,)
        if term == "a":
            a1 = + inner(grad(δv), J * D(v, Finv) * Finv.T) * dx

            # return (a1, a2, a3,)

            return (a1,)

        elif term == "c":
            c1 = - inner(δv, J * grad(self.u_) * Finv * (self.u_)) * dx
            c2 = + inner(δv, J * grad(self.u_) * Finv * self.w_) * dx

            c3 = +  inner(δS, dotEv(self.w_, 0.5*self.w_)) * dx
            c4 = -  inner(δdotEv(self.w_, 0.5*self.w_), S) * dx

            c = c1 + c2 + c3  +  c4 #+ c4
            #a2 =  + inner(δS, dotEv(w, d)) * dx
            #a3 =  - inner(δdotEv(δw, d), S) * dx

            return (c,)

        elif term == "dc":
            psiq = TrialFunction(M)
            c1 = - inner(δv, J * grad(self.u_) * Finv * (self.u_)) * dx
            c2 = + inner(δv, J * grad(self.u_) * Finv * self.w_) * dx
            c3 = +  inner(δS, dotEv(self.w_, 0.5*self.w_)) * dx
            c4 = -  inner(δdotEv(self.δw, 0.5*self.w_), S) * dx

            c = derivative(c1 + c2 + c3+ c4, self._solution,psiq)
            #c = derivative(c1 + c2 + c3, self._solution,psiq)

            return (c,)

        elif term == "b":
            b = - inner(grad(δv), J * p * I * Finv.T) * dx
            return (b,)

        elif term == "bt":
            bt = - inner(δp, div(J * Finv * v)) * dx
            return (bt,)

        elif term == "f":
            f0 = inner(δv,  traction) * ds(self.outlet)
            #    - inner(δv, J * force) * dx(self.elfluid)
            return (f0,)

        elif term == "g":
            g0 = inner(δv, J * force) * dx(self.elfluid)
            return (g0,)

        # BC condition..
        elif term == "dirichlet_bc_vel":
            bc0 = [DirichletBC(self.M.sub(0),  self.v_lid, boundaries, self.lid),
                   DirichletBC(self.M.sub(0),  self.v_wall,
                               boundaries, self.side),
                   DirichletBC(self.M.sub(0),  self.v_wall, boundaries, self.bottom)]
            return (bc0,)
        elif term == "dirichelt_bc_p":
            bc0 = [DirichletBC(self.M.sub(
                1), self.p_point, "std::abs(x[0])<1e-4 && std::abs(x[1]-1)<1e-8", method='pointwise')]
            return (bc0,)
        elif term == "dirichlet_bc_mvel":
            bc0 = [DirichletBC(self.M.sub(2).sub(1), Constant(0), boundaries, self.lid),
                   DirichletBC(self.M.sub(2).sub(1), Constant(0),
                               boundaries, self.bottom),
                   DirichletBC(self.M.sub(2).sub(0), Constant(0),
                               boundaries, self.side),
                   DirichletBC(self.M.sub(2), self.w_mid, boundaries, self.mid)]
            return (bc0,)
        elif term == "inner_product_vel":
            v = self.v
            δv = self.δv
            x0 = inner(grad(v), grad(δv))*self.dx
            return (x0,)
        elif term == "inner_product_p":
            p = self.p
            δp = self.δp
            x0 = inner(p, δp)*self.dx
            return (x0,)

        elif term == "inner_product_mvel":
            w = self.w
            δw = self.δw
            x0 = inner(grad(w), grad(δw))*self.dx
            return (x0,)
        elif term == "inner_product_sigmax":
            Sx = self.Sx
            δSx = self.δSx
            x0 = inner(Sx, δSx)*self.dx
            return (x0,)
        elif term == "inner_product_sigmay":
            Sy = self.Sy
            δSy = self.δSy
            x0 = inner(Sy, δSy)*self.dx
            return (x0,)

        elif term == "inner_product_sigmaxy":
            Sxy = self.Sxy
            δSxy = self.δSxy
            x0 = inner(Sxy, δSxy)*self.dx
            return (x0,)

        else:
            raise ValueError("Invalid term for assemble_operator().")


# 1. Read the mesh for this problem
# 1. Read the mesh for this problem
mesh = RectangleMesh(Point(0., 0.), Point(1., 1.), 50, 50)
boundaries = MeshFunction("size_t", mesh, mesh.topology().dim()-1)
boundaries.set_all(0)
l_ = 1.0
h_ = 1.0
x_l = CompiledSubDomain("near(x[0], side) && on_boundary", side=0.0)
x_r = CompiledSubDomain("near(x[0], side) && on_boundary", side=l_)
x_b = CompiledSubDomain("near(x[1], side) && on_boundary", side=0.0)
x_t = CompiledSubDomain("near(x[1], side) && on_boundary", side=h_)
x_m = CompiledSubDomain("near(x[0], side)               ", side=l_/2)
#
lid = 1
outlet = 2
wall = 3
mid = 4
elfluid = 0
side = 5
bottom = 6
x_t.mark(boundaries, lid)
x_r.mark(boundaries, side)
x_l.mark(boundaries, side)
x_b.mark(boundaries, bottom)
x_m.mark(boundaries, mid)

# 2. Create Finite Element space for Stokes problem (Taylor-Hood P2-P1-P2-DG1)
element_vel = VectorElement("Lagrange", mesh.ufl_cell(), 2)
element_p = FiniteElement("Lagrange", mesh.ufl_cell(), 1)
element_mvel = VectorElement("Lagrange", mesh.ufl_cell(), 2)
element_dg = FiniteElement("DG", mesh.ufl_cell(), 1)

element = MixedElement(element_vel, element_p, element_mvel,
                       element_dg, element_dg, element_dg)
M = FunctionSpace(mesh, element, components=[
                  ["vel", "s"], "p", "mvel", "sigmax", "sigmay", "sigmaxy"])

subdomains = MeshFunction("size_t", mesh, mesh.topology().dim())
subdomains.set_all(0)
print("pippo")
# 3. Allocate an object of the PseudoNavierStokes class
pseudonavierstokes_problem = PseudoNavierStokes(
    M, subdomains=subdomains, boundaries=boundaries)
mu_range = [(1e-2, 1e-1), (1.0, 1.0)]
pseudonavierstokes_problem.set_mu_range(mu_range)

# 4. Prepare reduction with a POD-Galerkin method
pod_galerkin_method = PODGalerkin(pseudonavierstokes_problem)
pod_galerkin_method.set_Nmax(20)

# 5. Perform the offline phase
pod_galerkin_method.initialize_training_set(
    100, sampling=LogUniformDistribution())
reduced_pseudonavierstokes_problem = pod_galerkin_method.offline()
"""
# TODO FB: I would stop here, have a look at the snapshots and confirm that they are ok before going any further

# 6. Perform an online solve
online_mu = (1e-2, 1e-1)
reduced_peudonavierstokes_problem.set_mu(online_mu)
reduced_peudonavierstokes_problem.solve()
peudonavierstokes_problem.export_solution(filename="online_solution")

# 7. Perform an error analysis
pod_galerkin_method.initialize_testing_set(
    100, sampling=LogUniformDistribution())
pod_galerkin_method.error_analysis()

# 8. Perform a speedup analysis
pod_galerkin_method.speedup_analysis()
"""

